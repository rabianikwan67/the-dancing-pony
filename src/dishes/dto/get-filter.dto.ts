import {IsNotEmpty, IsString} from "class-validator";

export class GetFilterDto {
    @IsNotEmpty()
    @IsString()
    search?: string
}