import {IsNotEmpty, IsNumber, IsString} from "class-validator";

export class UpdateDishDto {
    @IsString()
    name: string;

    @IsString()
    description: string;

    @IsNumber()
    price: number;
}