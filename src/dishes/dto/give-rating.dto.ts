import {IsNotEmpty, IsNumber, IsString, Max, Min} from "class-validator";

export class GiveRatingDto {
    @IsNumber()
    @IsNotEmpty()
    id: number;

    @IsNotEmpty()
    @IsNumber()
    @Min(1)
    @Max(5)
    rating: number
}