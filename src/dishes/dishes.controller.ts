import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    Query,
    Request,
    UseGuards
} from "@nestjs/common";
import {DishesService} from "./dishes.service";
import {Dishes} from "./model/dishes.model";
import {CreateDishDto} from "./dto/create-dish.dto";
import {UpdateDishDto} from "./dto/update-dish.dto";
import {GiveRatingDto} from "./dto/give-rating.dto";
import {User } from "../auth/user.entity";
import {GetFilterDto} from "./dto/get-filter.dto";
import {DishRepository} from "./repository/dish.repository";
import {Pagination} from "../paginate";
import { AuthGuard } from "@nestjs/passport";
import { GetUser } from "../auth/decorator/get-user.decorator";

// @UseGuards(AuthGuard('jwt'))
@Controller('api/v1/dishes')
export class DishesController {
    constructor(private dishesService: DishesService) {}

    // pagination
    @Get()
    async index(@Request() request): Promise<Pagination<Dishes>> {
        try {
            return await this.dishesService.pagination({
                limit: request.query.hasOwnProperty('limit') ? request.query.limit : 10,
                page: request.q.hasOwnProperty('page') ? request.query.page : 0,
            });
        } catch (e) {
            throw new BadRequestException()
        }
    }

    //search by keyword => /api/v1/dishes/find?search="keyword'
    @Get('/find')
    getTasks(
      @Query() filterDto: GetFilterDto,
    ): Promise<Dishes[]> {
        try {
            return this.dishesService.getDishes(filterDto);
        } catch (e) {
            throw new BadRequestException(["search by keyword => /api/v1/dishes/find?search='keyword'"])
        }
    }

    @Post()
    async createDish(@Body() dto: CreateDishDto): Promise<Dishes> {
        return await this.dishesService.createDish(dto)
    }

    @Get('/:id')
    async getDishById(@Param('id') id:number) {
        return await this.dishesService.getDishByName(id)
    }

    @Put('/:id')
    async updateDish(@Param('id') id:number, @Body() dto: UpdateDishDto) {
        return await this.dishesService.updateDish(id, dto)
    }

    // edit dish by endpoint /api/v1/dishes/:id_dish
    @Delete('/:name')
    async deleteDish(@Param('name') name:string) {
        return await this.dishesService.deleteDish(name)
    }

    //give rating by send json { id, rating }
    @Put()
    async giveRating(@Body() dto: GiveRatingDto, @GetUser() user: User) {
        console.log(user)
        return await this.dishesService.giveRating(dto, user)
    }

}
