import { Module } from '@nestjs/common';
import { DishesService } from './dishes.service';
import { DishesController } from './dishes.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Dishes} from "./model/dishes.model";
import {DishRepository} from "./repository/dish.repository";
import {ConfigModule} from "@nestjs/config";
import {Throttle, ThrottlerModule} from "@nestjs/throttler";

@Module({
  imports: [TypeOrmModule.forFeature([Dishes]),
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 10
    })
  ],
  providers: [DishesService, DishRepository],
  controllers: [DishesController]
})
export class DishesModule {}
