import {Injectable, NotFoundException, UnauthorizedException} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {Dishes} from "../model/dishes.model";
import {Repository} from "typeorm";
import {CreateDishDto} from "../dto/create-dish.dto";
import {UpdateDishDto} from "../dto/update-dish.dto";
import {GiveRatingDto} from "../dto/give-rating.dto";
import { User } from "../../auth/user.entity";
import {GetFilterDto} from "../dto/get-filter.dto";
import {Pagination, PaginationOptionsInterface} from "../../paginate";

@Injectable()
export class DishRepository {
    constructor(
        @InjectRepository(Dishes)
        private readonly dishesRepository: Repository<Dishes>
    ) {}

    async findById(id: number){
        const data:Dishes = await this.dishesRepository.findOneBy({ id })
        if (!data) {
            throw new NotFoundException('Dish Not Found')
        }
        return data
    }

    async getAll() {
        return await this.dishesRepository.find()
    }

    async findAll(dto: GetFilterDto): Promise<Dishes> {
        const { search} = dto;
        const query = this.dishesRepository.createQueryBuilder('dishes');
        if (search) {
            query.andWhere(
                '(LOWER(dishes.name) LIKE LOWER(:search) OR LOWER(dishes.description) LIKE LOWER(:search))',
                { search: `%${search}%` },
            );
        }
        return await query.getOne();
    }

    async createDish(dto: CreateDishDto): Promise<Dishes> {
        const { name , description, price } = dto;
        const dish: Dishes = this.dishesRepository.create({
            name, description, price
        })
        await this.dishesRepository.save(dish)
        return dish
    }

    async updateDish(id: number, dto: UpdateDishDto):Promise<Dishes> {
        const dish = await this.findById(id)
        dish.name = dto?.name;
        dish.description = dto?.description;
        dish.price = dto?.price;
        await this.dishesRepository.save(dish)
        return dish
    }

    async giveRating(dto:GiveRatingDto, user: User) {
        console.log(user)
        const dish = await this.findById(dto.id)
        if (dish.rating) {
            throw new UnauthorizedException('Rating has been made')
        }
        dish.rating = dto.rating;
        await this.dishesRepository.save(dish)
        return dish
    }

    async paginate(
        options: PaginationOptionsInterface,
    ): Promise<Pagination<Dishes>> {
        const [results, total] = await this.dishesRepository.findAndCount({
            take: options.limit,
            skip: options.page, // think this needs to be page * limit
        });
        // TODO add more tests for paginate
        return new Pagination<Dishes>({
            results,
            total,
        });
    }

    async deleteByName(name: string) {
        const result = await this.dishesRepository.delete({ name })
        if (result.affected === 0) {
            throw new NotFoundException('Dish Not Found')
        }
        return [{
            message: "dish has been deleted"
        }]
    }
}