import {Injectable, NotFoundException, UnauthorizedException} from '@nestjs/common';
import {Dishes} from "./model/dishes.model";
import {CreateDishDto} from "./dto/create-dish.dto";
import {UpdateDishDto} from "./dto/update-dish.dto";
import {GiveRatingDto} from "./dto/give-rating.dto";
import {DishRepository} from "./repository/dish.repository";
import {GetFilterDto} from "./dto/get-filter.dto";
import { User } from "../auth/user.entity";
import {PaginationOptionsInterface} from "../paginate";

@Injectable()
export class DishesService {
    constructor( private dishEntityRepository: DishRepository
    ) {}


    async getDishes(dto: GetFilterDto):Promise<Dishes[]> {
        return await this.dishEntityRepository.getAll()
    }

    async giveRating(dtp: GiveRatingDto, user: User) {
        return await this.dishEntityRepository.giveRating(dtp, user)
    }

    async pagination(option: PaginationOptionsInterface) {
        return await this.dishEntityRepository.paginate(option)
    }

    async getDishByName(id: number) :Promise<Dishes>{
        return await this.dishEntityRepository.findById(id)
    }

    async createDish(dto: CreateDishDto) :Promise<Dishes>{
        return await this.dishEntityRepository.createDish(dto)
    }

    async updateDish(id: number, dto: UpdateDishDto) :Promise<Dishes>{
        return await this.dishEntityRepository.updateDish(id, dto)
    }

    async deleteDish(name:string) {
        return await this.dishEntityRepository.deleteByName(name)
    }
}
