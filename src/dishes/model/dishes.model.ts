import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Dishes {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    name: string;

    @Column({ unique: true})
    description: string;

    @Column()
    price: number;

    @Column({ nullable: true})
    rating: number = null;
}
